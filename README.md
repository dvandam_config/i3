# i3

i3-wm configuration file.

## Requirements

This i3 configuration depends on:
* [alsa](https://alsa-project.org/wiki/Main_Page) - Sound framework.
* [light](https://github.com/haikarainen/light) - Controls backlights.
* [sakura](https://launchpad.net/sakura) - A simple libvte based terminal
  emulator.
* [rofi](https://github.com/DaveDavenport/rofi) - An application launcher.
* [rofi-menus](https://gitlab.com/vahnrr/rofi-menus) - Menus to be used with
  rofi.
* [polybar](https://github.com/polybar/polybar) - Highly customisable status
  bars.
* [betterlockscreen](https://github.com/pavanjadhaw/betterlockscreen) - Nice
  lockscreens.
* [pywal](https://github.com/dylanaraps/pywal/) - Generate colour schemes.
* [xwinwrap](https://github.com/ujjwal96/xwinwrap) - Stick apps to your desktop
  background.
* [compton](https://github.com/chjj/compton) - Composition manager

## Settings

The following settings are different from the default settings:
* Mod key is set to the super key.
* Killing a window is done with Mod+Shift+c.
* Reloading the configuration file is done with Mod+Shift+q.
* No window titles.

## Credits
* Inspiration and wallpapers,
  [1](https://www.reddit.com/r/unixporn/comments/cae6q8/bspwm_animeted_wallpaper_pywal/).
* GIF wallpapers, [1](https://imgur.com/gallery/0Slze),
  [2](https://www.pinterest.com/pin/608408230898081613/),
  [3](https://hetnice.blogspot.com/2019/09/gif-wallpaper-city.html),
